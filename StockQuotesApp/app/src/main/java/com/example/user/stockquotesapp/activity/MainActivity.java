package com.example.user.stockquotesapp.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.stockquotesapp.R;
import com.example.user.stockquotesapp.fragment.MainFragment;
import com.example.user.stockquotesapp.fragment.SymbolDetailFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.OnStockQuoteSelectedListener {

    private FragmentManager mFragmentManager;

    private FragmentTransaction mTransaction;

    private MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentManager= getFragmentManager();

        mainFragment= new MainFragment();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, mainFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onStockQuoteSelected(int position) {

        SymbolDetailFragment symbolDetailFragment = new SymbolDetailFragment();

        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, symbolDetailFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
