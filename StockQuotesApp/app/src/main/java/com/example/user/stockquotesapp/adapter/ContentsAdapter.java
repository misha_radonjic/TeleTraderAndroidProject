package com.example.user.stockquotesapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.stockquotesapp.R;
import com.example.user.stockquotesapp.data.Symbol;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 26.11.2016.
 */
public class ContentsAdapter extends ArrayAdapter<Symbol> {

    private Context mContext;
    private int mResource;
    private LayoutInflater mInflater;
    private ArrayList<Symbol> mSymbolList;


    public ContentsAdapter(Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mResource = resource;
        this.mInflater = LayoutInflater.from(mContext);
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row= convertView;
        Holder holder;

        if (row==null){

            row=mInflater.inflate(mResource,parent,false);
            holder= new Holder();

            holder.companyname=(TextView)row.findViewById(R.id.companynamecontents);
            holder.changepercent=(TextView)row.findViewById(R.id.changepercentcontents);
            holder.percent=(TextView)row.findViewById(R.id.percentcontents);
            holder.last=(TextView)row.findViewById(R.id.lastcontents);
            holder.root=(RelativeLayout)row.findViewById(R.id.contentsroot);
            holder.time=(TextView)row.findViewById(R.id.timecontents);

            row.setTag(holder);

        }
        else {
            holder=(Holder) row.getTag();
        }

        Symbol symbol = mSymbolList.get(position);

        if (position%2==0){
            holder.root.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        }
        else {

            holder.root.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }

        holder.companyname.setText(symbol.name);

     //   Symbol.Quote quote= new Symbol.Quote();

        Symbol.Quote quote = symbol.new Quote();


        double change=Double.parseDouble(quote.change);

        DecimalFormat df = new DecimalFormat("0.00");

        String changepercentFormated= df.format(change);

        holder.changepercent.setText(changepercentFormated);

        if (change>0){

            holder.changepercent.setTextColor(mContext.getResources().getColor(R.color.percentgrow));
            holder.percent.setTextColor(mContext.getResources().getColor(R.color.percentgrow));
        }
        else if (change<0){

            holder.changepercent.setTextColor(mContext.getResources().getColor(R.color.percentdecline));
            holder.percent.setTextColor(mContext.getResources().getColor(R.color.percentdecline));

        }
        else {
            holder.changepercent.setTextColor(mContext.getResources().getColor(R.color.percentstill));
            holder.percent.setTextColor(mContext.getResources().getColor(R.color.percentstill));
        }

        Double last= Double.parseDouble(quote.last);
        String lastFormated=df.format(last);

        holder.last.setText(lastFormated);

        if(mContext.getResources().getConfiguration().orientation==2) {
            holder.time.setVisibility(View.VISIBLE);

            String dateTime= quote.dateTime;
            String dateTimeFormated;

            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("HH:mm:ss");
            Date date= null;
            try {
                date = simpleDateFormat.parse(dateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dateTimeFormated= simpleDateFormat.format(date);

            holder.time.setText(dateTimeFormated);

        }

        return row;


    }

    private class Holder{
        public RelativeLayout root;
        public TextView companyname;
        public TextView changepercent;
        public TextView percent;
        public TextView last;
        public TextView time;


    }


}
