package com.example.user.stockquotesapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by User on 27.11.2016.
 */
public class UpdateService extends Service {

    private static final String TAG = "UpdateService";
    public static final String SERVICE_ACTION = "updatestockdata";
    private final Handler handler = new Handler();
    Intent intent;


    @Override
    public void onCreate() {
        super.onCreate();

        intent = new Intent(SERVICE_ACTION);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handler.removeCallbacks(sendUpdates);
        handler.postDelayed(sendUpdates, 1000);

    }

    private Runnable sendUpdates = new Runnable() {
        public void run() {
            showUpdates();
            handler.postDelayed(this, 300000);
        }
    };

    private void showUpdates() {

        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(sendUpdates);
        super.onDestroy();
    }
}
