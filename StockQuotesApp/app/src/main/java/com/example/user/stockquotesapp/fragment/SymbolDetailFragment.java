package com.example.user.stockquotesapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.stockquotesapp.R;
import com.example.user.stockquotesapp.data.Symbol;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by User on 26.11.2016.
 */
public class SymbolDetailFragment extends android.app.Fragment {

    private View mRootView;

    private ImageView mIconBack;

    private TextView mTitle;
    private TextView mDetailLast;
    private TextView mDetailChange;
    private TextView mDetailChangePercent;
    private TextView mDetailDateTime;
    private TextView mDetailOpen;
    private TextView mDetailHigh;
    private TextView mDetailLow;
    private TextView mDetailVolume;
    private TextView mDetailBid;
    private TextView mDetailAsk;
    private TextView mDetailPercentSymbol;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();

        addListeners();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView= inflater.inflate(R.layout.fragment_symboldetail,container,false);

        return mRootView;
    }

    private void initComponents(){

        mIconBack=(ImageView)mRootView.findViewById(R.id.symboldetailiconback);

        mTitle=(TextView)mRootView.findViewById(R.id.symboldetailtitle);
        mDetailLast=(TextView)mRootView.findViewById(R.id.symboldetaillast);
        mDetailChange=(TextView)mRootView.findViewById(R.id.symboldetailchange);
        mDetailChangePercent=(TextView)mRootView.findViewById(R.id.changepercentsymboldetails);

        mDetailDateTime=(TextView)mRootView.findViewById(R.id.symboldetaildatetime);
        mDetailOpen=(TextView)mRootView.findViewById(R.id.symboldetailopen);
        mDetailHigh=(TextView)mRootView.findViewById(R.id.symboldetailhigh);
        mDetailLow=(TextView)mRootView.findViewById(R.id.symboldetaillow);
        mDetailVolume=(TextView)mRootView.findViewById(R.id.symboldetailvolume);
        mDetailBid=(TextView)mRootView.findViewById(R.id.symboldetailbid);
        mDetailAsk=(TextView)mRootView.findViewById(R.id.symboldetailask);
        mDetailPercentSymbol=(TextView)mRootView.findViewById(R.id.percentsymboldetails);

        Bundle bundle = getArguments();

        Symbol symbol= (Symbol) bundle.getSerializable("symbol");

        Symbol.Quote quote = symbol.new Quote();

        mTitle.setText(symbol.name);

        double detaillast=Double.parseDouble(quote.last);

        DecimalFormat df = new DecimalFormat("0.0000");

        String detaillastFormated= df.format(detaillast);

        mDetailLast.setText(detaillastFormated);

        Double change= Double.parseDouble(quote.change);
        String changeFormated=df.format(change);

        mDetailChange.setText(changeFormated);

        Double changepercent= Double.parseDouble(quote.changePercent);
        String changepercentFormated=df.format(changepercent);

        mDetailChangePercent.setText(changepercentFormated);

        Double open= Double.parseDouble(quote.open);
        String openFormated=df.format(open);

        mDetailOpen.setText(openFormated);

        Double high= Double.parseDouble(quote.high);
        String highFormated=df.format(high);

        mDetailHigh.setText(highFormated);

        Double low= Double.parseDouble(quote.low);
        String lowFormated=df.format(low);

        mDetailLow.setText(lowFormated);

        Double volume= Double.parseDouble(quote.volume);
        String volumeFormated=df.format(volume);

        mDetailVolume.setText(volumeFormated);

        Double bid= Double.parseDouble(quote.bid);
        String bidFormated=df.format(bid);

        mDetailBid.setText(bidFormated);

        Double ask= Double.parseDouble(quote.ask);
        String askFormated=df.format(ask);

        mDetailAsk.setText(askFormated);

        if (detaillast>0){

            mDetailLast.setTextColor(getActivity().getResources().getColor(R.color.percentgrow));
            mDetailChange.setTextColor(getActivity().getResources().getColor(R.color.percentgrow));
            mDetailChangePercent.setTextColor(getActivity().getResources().getColor(R.color.percentgrow));
            mDetailPercentSymbol.setTextColor(getActivity().getResources().getColor(R.color.percentgrow));

        }
        else if (detaillast<0){

            mDetailLast.setTextColor(getActivity().getResources().getColor(R.color.percentdecline));
            mDetailChange.setTextColor(getActivity().getResources().getColor(R.color.percentdecline));
            mDetailChangePercent.setTextColor(getActivity().getResources().getColor(R.color.percentdecline));
            mDetailPercentSymbol.setTextColor(getActivity().getResources().getColor(R.color.percentdecline));


        }
        else {
            mDetailLast.setTextColor(getActivity().getResources().getColor(R.color.percentstill));
            mDetailChange.setTextColor(getActivity().getResources().getColor(R.color.percentstill));
            mDetailChangePercent.setTextColor(getActivity().getResources().getColor(R.color.percentstill));
            mDetailPercentSymbol.setTextColor(getActivity().getResources().getColor(R.color.percentstill));

        }

        String dateTime= quote.dateTime;
        String dateTimeFormated;

        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("dd.MM.yyy HH:mm:ss");
        Date date= null;
        try {
            date = simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateTimeFormated= simpleDateFormat.format(date);

        mDetailDateTime.setText(dateTimeFormated);






    }

    private void addListeners(){

        mIconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().finish();
            }
        });
    }
}
