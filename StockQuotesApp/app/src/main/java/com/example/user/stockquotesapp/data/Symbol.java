package com.example.user.stockquotesapp.data;

import java.io.Serializable;

/**
 * Created by User on 26.11.2016.
 */
public class Symbol implements Serializable{

    public String id;
    public String name;

    public class Quote {

        public  String last;
        public  String open;
        public  String high;
        public String low;
        public String bid;
        public String ask;
        public String volume;
        public String dateTime;
        public String change;
        public String changePercent;


   }


}
