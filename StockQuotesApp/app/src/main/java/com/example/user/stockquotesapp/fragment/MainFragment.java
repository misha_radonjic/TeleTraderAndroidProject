package com.example.user.stockquotesapp.fragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.IntentService;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.stockquotesapp.R;
import com.example.user.stockquotesapp.adapter.ContentsAdapter;
import com.example.user.stockquotesapp.container.SymbolContainer;
import com.example.user.stockquotesapp.data.Symbol;
import com.example.user.stockquotesapp.service.UpdateService;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by User on 26.11.2016.
 */
public class MainFragment extends ListFragment {

    OnStockQuoteSelectedListener mCallback;

    String URL="http://www.teletrader.com/downloads/symbols.xml";

    public static final String SYMBOL_ID="id";
    public static final String SYMBOL_NAME="name";
    public static final String SYMBOL_QUOTE_LAST="last";
    public static final String SYMBOL_QUOTE_OPEN="open";
    public static final String SYMBOL_QUOTE_HIGH="high";
    public static final String SYMBOL_QUOTE_LOW="low";
    public static final String SYMBOL_QUOTE_BID="bid";
    public static final String SYMBOL_QUOTE_ASK="ask";
    public static final String SYMBOL_QUOTE_VOLUME="volume";
    public static final String SYMBOL_QUOTE_DATETIME="dateTime";
    public static final String SYMBOL_QUOTE_CHANGE="change";
    public static final String SYMBOL_QUOTE_CHANGEPERCENT="changePercent";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor preferencesEditor;
    private static final int PREFERENCES_MODE_PRIVATE=0;

    public interface OnStockQuoteSelectedListener {
         void onStockQuoteSelected(int position);
    }

    private View mRootView;
    private ListView mListView;
    private ProgressBar mProgressBar;
    private ContentsAdapter adapter;
    private ArrayList<Symbol> mSymbolList;
    public NodeList nodelist;
    private Loaddata loadData;

    private Intent intent;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadData=new Loaddata();
        loadData.execute(URL);
        sharedPreferences=getActivity().getPreferences(PREFERENCES_MODE_PRIVATE);
        preferencesEditor=sharedPreferences.edit();
        intent = new Intent(getActivity().getApplicationContext(), UpdateService.class);
      //  new Loaddata().execute(URL);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
        //addListeners();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        mRootView=inflater.inflate(R.layout.fragment_main,container,false);

        return mRootView;
    }

    private void initComponents(){

        mListView=(ListView) mRootView.findViewById(R.id.listviewcontents);
        mProgressBar=(ProgressBar) mRootView.findViewById(R.id.progressBarcontents);
     /*   mSymbolList=SymbolContainer.SymbolList;
        adapter= new ContentsAdapter(getActivity().getApplicationContext(),R.layout.list_item_fragment_main, mSymbolList);
        mListView.setAdapter(adapter); */

    }

  /*  private void addListeners(){


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                SymbolDetailFragment symbolDetailFragment = new SymbolDetailFragment();
                android.app.FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, symbolDetailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();


            }
        });
    } */

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnStockQuoteSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        SymbolDetailFragment symbolDetailFragment = new SymbolDetailFragment();
        Bundle bundle = new Bundle();
        Symbol symbol= mSymbolList.get(position);

        if (symbol != null) {

            bundle.putSerializable("symbol",symbol);


        } else {
            Log.e("symbol", "is null");

        }

        symbolDetailFragment.setArguments(bundle);


        fragmentTransaction.replace(R.id.container, symbolDetailFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }



    class Loaddata extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Toast.makeText(getContext(),"Downloading latest Stock quotes",Toast.LENGTH_LONG).show();

            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {


            URL url = null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            DocumentBuilderFactory dbf = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder db = null;
            try {
                db = dbf.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            // Download the XML file
            Document doc = null;
            try {
                doc = db.parse(new InputSource(url.openStream()));
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            doc.getDocumentElement().normalize();
            // Locate the Tag Name
            nodelist = doc.getElementsByTagName(SYMBOL_ID);

            return null;


        }

        @Override
        protected void onPostExecute(String s) {

            mProgressBar.setVisibility(View.GONE);


            for (int i=0;i< nodelist.getLength();i++){

                Symbol symbol= (Symbol)nodelist.item(i);
                mSymbolList.add(symbol);

            }


            adapter= new ContentsAdapter(getActivity().getApplicationContext(),R.layout.list_item_fragment_main, mSymbolList);
            mListView.setAdapter(adapter);



        }
    }

    @Override
    public void onDestroy() {
        loadData.cancel(true);
        super.onDestroy();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadData.execute(URL);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getApplicationContext().startService(intent);
        getActivity().getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter(UpdateService.SERVICE_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getApplicationContext().unregisterReceiver(broadcastReceiver);
        getActivity().getApplicationContext().stopService(intent);
    }
}
